<!DOCTYPE html>
<html>
	<head>
		<meta charset="utf-8"/>
		<title>Diana Dueñas</title>
		<link type="text/css" rel="stylesheet" href="css/materialize.min.css"  media="screen,projection"/>
		<meta name="viewport" content="width=device-width, initial-scale=1.0"/>
	</head>

	<body>
		<!--JQUERY AND MATERIALIZE INIT-->
		<script type="text/javascript" src="https://code.jquery.com/jquery-2.1.1.min.js"></script>
		<script type="text/javascript" src="js/materialize.min.js"></script>

		<h1 class="card-panel purple lighten-1 center-align white-text">Intro a PHP</h1>
		<div class="section">


			<h3 class="center-align text-deep-purple lighten-5">Funciones</h3>
			<div class="divider"></div>

			<?php 
$numbers = array(2,3,3,4,5,2,1,3,5,5,7,8);

function printarr($arreglo){

	foreach ($arreglo as $arg) {
		echo $arg." ";					

	}
	echo "</br>";


}
			?>


			<h4>Función promedio</h4>
			<h6>Arreglo de Prueba</h6>

			<?php
printarr($numbers);

			?>

			<p>
				<?php
function promedio($arreglo) {
	$suma = 0;
	$contador = 0;
	$promedio = 0;
	foreach ($arreglo as $arr) {
		$suma+=$arr;
		$contador++;
	}
	$promedio=$suma/$contador;
	echo "El promedio es: ".$promedio;
}

promedio($numbers);


				?>
			</p>

			<h4>Función mediana</h4>
			<h6>Arreglo de Prueba</h6>

			<?php
printarr($numbers);

			?>


			<p>
				<?php
function mediana($arreglo) {


	if(sort ( $arreglo, SORT_NUMERIC ))
		echo "La mediana es: ".$arreglo[count($arreglo)/2];
	else
		echo "ERROR";

}

mediana($numbers);


				?>
			</p>

			<h4>Función Máxima</h4>

			<h6>Arreglo de Prueba</h6>

			<?php
$nuevoarreglo = array(7,0,8,9,-2,4,5,1,3);

function maximus($nuevoarreglo) {

	printarr($nuevoarreglo);

	echo "<ol>";

	if(sort ( $nuevoarreglo, SORT_NUMERIC )){
		echo "<li>Orden ascendente: ";
		printarr($nuevoarreglo);
		echo "</li>";
		echo "<li>Orden descendente: ";
		for($i=count($nuevoarreglo)-1;$i>=0;$i--){
			echo $nuevoarreglo[$i]." ";
		}
		echo "</li>";
		echo "<li>";
		promedio($nuevoarreglo);
		echo "</li>";
		echo "<li>";
		mediana($nuevoarreglo);
		echo "</li>";
	}else{
		echo "ERROR";
	}

	echo "</ol>";
}

maximus($nuevoarreglo);


			?>

		</div>
		<div class="section center-align">

			<h3 class="center-align">Tabla de 1 a n</h3>
			<div class="divider"></div>
			<p>n = <?php $n=floor(rand(0,20));
echo $n;?></p>
			<table class="bordered">
				<tr>
					<th>Número</th>
					<th>Cuadrado</th>
					<th>Cubo</th>
				</tr>
				<?php 
for($i=1;$i<=$n;$i++){
	echo "<tr>";
	echo "<td>".$i."</td>";
	echo "<td>".pow($i,2)."</td>";
	echo "<td>".pow($i,3)."</td>";
	echo "</tr>";
}
				?>
			</table>	
		</div>
		<div class="section center-align">
			<h3 class="center-align">Codeforces Problem</h3>
			<div class="divider"></div>
			<h4>Theatre Square</h4>
			<p>Theatre Square in the capital city of Berland has a rectangular shape with the size n × m meters. On the occasion of the city's anniversary, a decision was taken to pave the Square with square granite flagstones. Each flagstone is of the size a × a.

				What is the least number of flagstones needed to pave the Square? It's allowed to cover the surface larger than the Theatre Square, but the Square has to be covered. It's not allowed to break the flagstones. The sides of flagstones should be parallel to the sides of the Square.

				<strong>Input</strong>
				The input contains three positive integer numbers in the first line: n,  m and a (1 ≤  n, m, a ≤ 109).

				<strong>Output</strong>
				Write the needed number of flagstones.</p>

			<?php 

$n_=8;
$m_=9;
$a_=4;

echo "<strong>Sample Input: </strong>n-".$n_." m-".$m_." a-".$a_."</br></br>";

function theatreSquare($n,$m,$a){

	$cuadritos=0;

	if($a!=0){
		$cuadritos=ceil($n/$a)*ceil($m/$a);
	}

	echo $cuadritos." flagstones are needed";
}

theatreSquare($n_,$m_,$a_);

			?>
		</div>



	</body>
</html>